﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadioNetworksGraphViewer
{
    #region EXCEPTIONS
class NodeNotFoundExeption : Exception
{
    public NodeNotFoundExeption()
    :base("Node with ID does not exist.")
    {

    }
};
class IdAlreadyUsedException : Exception
{
    public IdAlreadyUsedException()
    :base("ID selected is already used by another node.")
    {

    }
};
class NotImplementedException : Exception
{
    public NotImplementedException()
    :base("This function is not implemented.")
    {

    }
};
class NotValidInputException : Exception
{
    public NotValidInputException()
    :base("The input values are not valid.")
    {

    }
};
class WrongMatrixSizeException : Exception
{
    public WrongMatrixSizeException()
    :base("The size of the input matrix is wrong.")
    {

    }
};
class OperationNotPossibleException : Exception
{
    public OperationNotPossibleException()
    :base("This operation is not possible.")
    {

    }
};
#endregion
    [Serializable]
    class Vec3D{
        public Vec3D(double x,double y,double z){
            this.x = x;
            this.y = y;
            this.z = z;
        }
	    public Vec3D(){
            this.x = 0;
            this.y = 0;
            this.z = 0;
        }
	    public double x;
        public double y;
        public double z;

	    public static Vec3D operator+(Vec3D v1,Vec3D v2){
            return new Vec3D(v2.x + v1.x,v2.y + v1.y,v2.z + v1.z);
        }
	    public static Vec3D operator-(Vec3D v1,Vec3D v2){
            return new Vec3D(v1.x - v2.x,v1.y - v2.y,v1.z - v2.z);
        }
        public static double operator*(Vec3D v1,Vec3D v2){
            return v2.x * v1.x + v2.y * v1.y + v2.z * v1.z;
        }
	    public static Vec3D operator*(Vec3D v, double s){
            return new Vec3D(v.x*s,v.y*s,v.z*s);
        }
	    public static Vec3D operator/(Vec3D v, double s){
            return v*(1/s);
        }
	    public Vec3D getVersor(){
            double mod = this.module();
            return this/mod;
        }	
	    public double module2(){return x*x+y*y+z*z;}
	    public double module(){ return Math.Sqrt(module2());}
};

    class Matrix{
        private	int _rows;
        private int _columns;
        private double[] _data;

	    public Matrix(){
            this._rows = 0;
	        this._columns = 0;
        }
	    public Matrix(int rows, int columns){
            this._rows = rows;
	        this._columns = columns;
	        this._data = new double[rows*columns];
        }
	    public Matrix(int size){
            this._rows = size;
	        this._columns = size;
	        this._data = new double[size*size];
        }
        public double GetElement(int row, int column)
        {
            if (row >= this._rows || column >= this._columns) throw new NotValidInputException();
            return this._data[row * this._columns + column];
        }
	    public int getRows(){ return _rows;}
	    public int getColumns(){ return _columns; }
	    public void SetElement(int row, int column, double value){
            if (row >= this._rows || column >= this._columns) throw new NotValidInputException();
	        this._data[row*this._columns + column] = value;
        }
	    public bool IsSymmetric(){
            if (!this.IsSquare()) return false;
	for (int row = 0; row < this._rows; row++){
		for (int col = row; col < this._columns; col++){
			if (this.GetElement(row, col) != this.GetElement(col, row)) return false;
		}
	}
	return true;
        }        
	    public Matrix GetColumn(int column){
            Matrix result = new Matrix(this._rows, 1);	
	        for (int i = 0; i < this._rows; i++) result.SetElement(i, 0, this.GetElement(i, column));
	        return result;
        }
	    public Matrix GetRow(int row){
            Matrix result = new Matrix(1, this._columns);
	        for (int i = 0; i < this._columns; i++) result.SetElement(0, i, this.GetElement(row,i));
	        return result;
        }
	    public double Sum(){
            double sum = 0;
	        for (int i = 0; i < this._data.Length; i++) sum += this._data[i];
	        return sum;
        }
	    public static Matrix operator +(Matrix m1, Matrix m2){
            if (m1._rows != m2._rows || m1._columns != m2._columns) throw new WrongMatrixSizeException();
	        Matrix result = new Matrix(m1._rows, m1._columns);
	        for (int i = 0; i < m1._data.Length; i++) result._data[i] = m1._data[i] + m2._data[i];
	        return result;
        }
	    public static Matrix operator -(Matrix m1, Matrix m2){
            if (m1._rows != m2._rows || m1._columns != m2._columns) throw new WrongMatrixSizeException();
	        Matrix result = new Matrix(m1._rows, m1._columns);
	        for (int i = 0; i < m1._data.Length; i++) result._data[i] = m1._data[i] - m2._data[i];
	        return result;
        }
	    public static Matrix operator *(Matrix m1, Matrix m2){
            if (m1._columns != m2._rows) throw new WrongMatrixSizeException();
	        Matrix result = new Matrix(m1._rows, m2._columns);
	
	        for (int row = 0; row < result._rows; row++){
		        for (int col = 0; col < result._columns; col++){
			        double sum = 0;
			        for (int i = 0; i < m1._columns; i++) sum += (m1.GetElement(row,i)*m2.GetElement(i,col));
			        result.SetElement(row, col, sum);
		        }
	        }
	
	        return result;
        }
	    public static Matrix operator *(Matrix m, double value){
            Matrix result = new Matrix(m._rows, m._columns);
	        for (int i = 0; i < m._data.Length; i++) result._data[i] = m._data[i] * value;
	        return result;
        }
	    public static Matrix operator /(Matrix m1, Matrix m2){
            return m1*(m2.GetInverse());
        }
	    public static Matrix operator /(Matrix m, double value){
            Matrix result = new Matrix(m._rows, m._columns);
	    for (int i = 0; i < m._data.Length; i++) result._data[i] = m._data[i] / value;
	    return result;
        }
	    public Matrix Transpose(){
            Matrix result = new Matrix(this._columns, this._rows);
	        for (int row = 0; row < result._rows; row++){
		        for (int col = 0; col < result._columns; col++){
			        result.SetElement(row, col, this.GetElement(col,row));
		        }
	        }
	        return result;
        }
	    public bool IsSquare(){return this._columns == this._rows;}
	    public double GetDeterminant(){throw new NotImplementedException();}
	    public Matrix GetInverse(){throw new NotImplementedException();}
	    public string ToString(){
            string result = "";
	    for (int row = 0; row < this._rows; row++){
		    for (int col = 0; col < this._columns; col++){
			    result += this.GetElement(row, col) + " ";
		    }
		    result += "\n";
	    }
	    return result;
        }

        public static Matrix Identity(int size){
            Matrix result = new Matrix(size);
	        for (int row = 0; row < result.getRows(); row++) for (int col = 0; col < result.getColumns(); col++) result.SetElement(row, col, row == col ? 1 : 0);
	        return result;
        }

        public static Matrix Zeros(int rows, int columns){
            Matrix result = new Matrix(rows, columns);
	        for (int row = 0; row < result.getRows(); row++) for (int col = 0; col < result.getColumns(); col++) result.SetElement(row, col, 0);
	        return result;
        }

        public static Matrix Zeros(int size){
            Matrix result = new Matrix(size);
	        for (int row = 0; row < result.getRows(); row++) for (int col = 0; col < result.getColumns(); col++) result.SetElement(row, col, 0);
	        return result;
        }
    }

    [Serializable]
    class Node{
        private Vec3D _position;
	    private string _name;
	    private double _transmissionRange;
	    private double _receiverSensitivity;
	    private double _transmitPower;
        private double _protectionRatio;

        public Node() { }
	    public void SetName(string name){this._name = name;}
	    public string GetName(){ return this._name;}
	    public void SetPosition(Vec3D position){this._position = position;}
	    public Vec3D GetPosition(){return this._position;}
	    /*public void SetTransmissionRange(double range){
            if (range>0) this._transmissionRange = range; 
	        else throw new NotValidInputException();
        }*/
	    public double GetTransmissionRange(){return this._transmissionRange;}
        public void SetProtectionRatio(double protection_ratio){ this._protectionRatio = protection_ratio; }
        public double GetProtectionRatio() { return this._protectionRatio; }
        private void UpdateTransmissionRange(double K, double beta)
        {
            this._transmissionRange = Math.Pow(10, (this._transmitPower - K - this._receiverSensitivity) / (10 * beta));
        }
	    public void SetReceiverSensitivity(double sensitivity, double K, double beta){
            this._receiverSensitivity = sensitivity;
            UpdateTransmissionRange(K, beta);
        }
	    public double GetReceiverSensitivity(){return this._receiverSensitivity;}
	    public void SetTransmitPower(double transmit_power, double K, double beta){
            this._transmitPower = transmit_power;
            UpdateTransmissionRange(K, beta);
        }
	    public double GetTransmitPower(){return this._transmitPower;}
    }

    class Edge{
	    private int _from;
	    private int _to;
	    private double _weight;

        public Edge() { }
	    public void SetFromNode(int node_index){this._from = node_index;}
	    public void SetToNode(int node_index){ this._to = node_index; }
	    public void SetWeight(double weight){ this._weight = weight; }
	    public int GetFromNode(){ return this._from; }
	    public int GetToNode(){ return this._to; }
	    public double GetWeight(){ return this._weight; }
    }

    class Graph{
	    private Matrix _adjacencyMatrix;
   
	    public Graph(){this._adjacencyMatrix = new Matrix(0);}
	    public Graph(int nodeNumber){this._adjacencyMatrix = Matrix.Identity(nodeNumber);}
	    public Graph(Matrix AdjacencyMatrix){this._adjacencyMatrix = AdjacencyMatrix;}
	    public Matrix GetAdjacencyMatrix(){return this._adjacencyMatrix;}
	    public Graph GetMinimumSpanningTreeDistances(){
            if (!this.IsUndirected()) throw new OperationNotPossibleException();

	        //initialization
	        int NodeNumber = this._adjacencyMatrix.getColumns();
	        int startNode = 0;		//may be an argument...


	        ArrayList T_nodes = new ArrayList();
            T_nodes.Add(startNode);
	        ArrayList G_T_nodes = new ArrayList();
	        for (int i = 0; i < NodeNumber; i++) if (i != startNode) G_T_nodes.Add(i);

            Graph T = new Graph(Matrix.Identity(NodeNumber));

	        for (int step = 0; step < NodeNumber; step++){
		        Edge minEdge = new Edge();
		        minEdge.SetWeight(double.MaxValue);

		        bool minEdgeUpdated = false;
		        for (int srcIdx = 0; srcIdx < T_nodes.Count; srcIdx++){
			        int source = (int)T_nodes[srcIdx];
			        for (int dstIdx = 0; dstIdx < G_T_nodes.Count; dstIdx++){
				        int destination = (int)G_T_nodes[dstIdx];
				        if (this._adjacencyMatrix.GetElement(source, destination) > 0){
					        if (minEdge.GetWeight() > this._adjacencyMatrix.GetElement(source, destination)){
						        minEdge.SetFromNode(source);
						        minEdge.SetToNode(destination);
						        minEdge.SetWeight(this._adjacencyMatrix.GetElement(source, destination));
						        minEdgeUpdated = true;
					        }
				        }
			        }
		        }

		        if (minEdgeUpdated){
			        T.SetWeight(minEdge.GetFromNode(), minEdge.GetToNode(), minEdge.GetWeight());
			        T.SetWeight(minEdge.GetToNode(), minEdge.GetFromNode(), minEdge.GetWeight());

			        T_nodes.Add(minEdge.GetToNode());
			        int indexToRemove = -1;
			        for (int i = 0; i < G_T_nodes.Count; i++) if (((int)G_T_nodes[i]) == minEdge.GetToNode()) indexToRemove = i;
                    G_T_nodes.RemoveAt(indexToRemove);
		        }
	        }
	        return T;
        }

        public Graph GetMinimumSpanningTreeRxPowers(Network net)
        {
            if (!this.IsUndirected()) throw new OperationNotPossibleException();

            //initialization
            int NodeNumber = this._adjacencyMatrix.getColumns();
            int startNode = 0;      //may be an argument...


            ArrayList T_nodes = new ArrayList();
            T_nodes.Add(startNode);
            ArrayList G_T_nodes = new ArrayList();
            for (int i = 0; i < NodeNumber; i++) if (i != startNode) G_T_nodes.Add(i);

            Graph T = new Graph(Matrix.Identity(NodeNumber));

            for (int step = 0; step < NodeNumber; step++)
            {
                Edge minEdge = new Edge();
                minEdge.SetWeight(double.MaxValue);

                bool minEdgeUpdated = false;
                for (int srcIdx = 0; srcIdx < T_nodes.Count; srcIdx++)
                {
                    int source = (int)T_nodes[srcIdx];
                    for (int dstIdx = 0; dstIdx < G_T_nodes.Count; dstIdx++)
                    {
                        int destination = (int)G_T_nodes[dstIdx];
                        if (this._adjacencyMatrix.GetElement(source, destination) > 0)
                        {
                            double rxPowers = (net.NodeSet[(int)T_nodes[srcIdx]].GetTransmitPower());
                            rxPowers = rxPowers - net.GetChannelK();
                            rxPowers = rxPowers - (10 * net.GetChannelBeta() * Math.Log10((double)this._adjacencyMatrix.GetElement(source, destination)));
                            if (minEdge.GetWeight() > rxPowers)
                            {
                                minEdge.SetFromNode(source);
                                minEdge.SetToNode(destination);

                                rxPowers = (net.NodeSet[(int)T_nodes[srcIdx]].GetTransmitPower());
                                rxPowers = rxPowers - net.GetChannelK();
                                rxPowers = rxPowers - (10 * net.GetChannelBeta() * Math.Log10((double)this._adjacencyMatrix.GetElement(source, destination)));
                                minEdge.SetWeight(-rxPowers);
                                minEdgeUpdated = true;
                            }
                        }
                    }
                }

                if (minEdgeUpdated)
                {
                    T.SetWeight(minEdge.GetFromNode(), minEdge.GetToNode(), minEdge.GetWeight());
                    T.SetWeight(minEdge.GetToNode(), minEdge.GetFromNode(), minEdge.GetWeight());

                    T_nodes.Add(minEdge.GetToNode());
                    int indexToRemove = -1;
                    for (int i = 0; i < G_T_nodes.Count; i++) if (((int)G_T_nodes[i]) == minEdge.GetToNode()) indexToRemove = i;
                    G_T_nodes.RemoveAt(indexToRemove);
                }
            }
            return T;
        }

        public Graph GetMinimumSpanningTreeWeightOne()
        {
            if (!this.IsUndirected()) throw new OperationNotPossibleException();

            //initialization
            int NodeNumber = this._adjacencyMatrix.getColumns();
            int startNode = 0;      //may be an argument...


            ArrayList T_nodes = new ArrayList();
            T_nodes.Add(startNode);
            ArrayList G_T_nodes = new ArrayList();
            for (int i = 0; i < NodeNumber; i++) if (i != startNode) G_T_nodes.Add(i);

            Graph T = new Graph(Matrix.Identity(NodeNumber));

            for (int step = 0; step < NodeNumber; step++)
            {
                Edge minEdge = new Edge();
                minEdge.SetWeight(Double.MaxValue);

                bool minEdgeUpdated = false;
                for (int srcIdx = 0; srcIdx < T_nodes.Count; srcIdx++)
                {
                    int source = (int)T_nodes[srcIdx];
                    for (int dstIdx = 0; dstIdx < G_T_nodes.Count; dstIdx++)
                    {
                        int destination = (int)G_T_nodes[dstIdx];
                        if (this._adjacencyMatrix.GetElement(source, destination) > 0)
                        {
                            if (minEdge.GetWeight() > 1)
                            {
                                minEdge.SetFromNode(source);
                                minEdge.SetToNode(destination);
                                minEdge.SetWeight(1);
                                minEdgeUpdated = true;
                            }
                        }
                    }
                }

                if (minEdgeUpdated)
                {
                    T.SetWeight(minEdge.GetFromNode(), minEdge.GetToNode(), minEdge.GetWeight());
                    T.SetWeight(minEdge.GetToNode(), minEdge.GetFromNode(), minEdge.GetWeight());

                    T_nodes.Add(minEdge.GetToNode());
                    int indexToRemove = -1;
                    for (int i = 0; i < G_T_nodes.Count; i++) if (((int)G_T_nodes[i]) == minEdge.GetToNode()) indexToRemove = i;
                    G_T_nodes.RemoveAt(indexToRemove);
                }
            }
            return T;
        }
        public bool IsUndirected(){return this._adjacencyMatrix.IsSymmetric();}
	    public bool IsComplete(){
            if (!this.IsUndirected()) throw new OperationNotPossibleException();
	        for (int col = 0; col < this._adjacencyMatrix.getColumns(); col++){
		        for (int row = 0; row < this._adjacencyMatrix.getRows(); row++){
			        if (this._adjacencyMatrix.GetElement(row, col) == 0) return false;
		        }
	        }
	        return true;
        }
	    public bool IsConnected(){return this.GetConnectedComponentNumber() == 1;}
	    public bool EdgeExists(int u, int v){
            if (u >= this._adjacencyMatrix.getRows() || v >= this._adjacencyMatrix.getColumns()) throw new NotValidInputException();
	        return this._adjacencyMatrix.GetElement(u, v)>0;
        }
        public int GetEdgeNumber()
        {
            int result = 0;
            for (int u = 0; u < this.GetNodeNumber(); u++)
            {
                for (int v = 0; v < this.GetNodeNumber(); v++)
                {
                    if (u != v)
                    {
                        if (EdgeExists(u, v))
                        {
                            result++;
                        }
                    }
                }
            }            
            return result;
        }
	    public int GetConnectedComponentNumber(){
            if (!this.IsUndirected()) throw new OperationNotPossibleException();
	        int components = 1;
	        ArrayList nodesConsidered = new ArrayList();
	        int nodeNumber = this._adjacencyMatrix.getColumns();
	        for (int n = 0; n < nodeNumber; n++){
		        bool considered = false;
		        for (int i = 0; i < nodesConsidered.Count; i++) if (((int)nodesConsidered[i]) == n) considered = true;
		        if (!considered){
			        nodesConsidered.Add(n);

			        bool somethingChanged = true;
			        while(somethingChanged){
				        somethingChanged = false;
				        for (int j = 0; j < nodesConsidered.Count; j++){
					        for (int dst = 0; dst < this._adjacencyMatrix.getColumns(); dst++){
						        int src = (int)nodesConsidered[j];
						        if (this.EdgeExists(src, dst)){
							        bool alreadyPresent = false;
							        for (int i = 0; i < nodesConsidered.Count; i++) if (((int)nodesConsidered[i]) == dst) alreadyPresent = true;
							        if (!alreadyPresent){
								        nodesConsidered.Add(dst);
								        somethingChanged = true;
							        }
						        }
					        }
				        }
			        }
			        if (nodesConsidered.Count < nodeNumber) components++;
			        else return components;
		        }
	        }
	        return components;
        }
	    public int GetNodeNumber(){return this._adjacencyMatrix.getColumns();}
	    public double GetWeight(int u, int v){
            if (u >= this.GetNodeNumber() || v >= this.GetNodeNumber()) throw new NotValidInputException();
	        return this._adjacencyMatrix.GetElement(u, v);
        }
	    public void SetWeight(int u, int v, double weight){
            if (u >= this.GetNodeNumber() || v >= this.GetNodeNumber()) throw new NotValidInputException();
	        this._adjacencyMatrix.SetElement(u, v, weight);
        }
    }

    [Serializable]
    class Network {
        public List<Node> NodeSet;
        public List<Node> LinkSet;
        private double[] RandomSamples;
	    private void _packIDs(){}
        private double K, beta;
        private Random rg;
    
	    public Network(){
            NodeSet = new List<Node>();
            LinkSet = null;
            rg = new Random(1234);
            RandomSamples = new double[50];

            for (int i = 0; i < RandomSamples.Length; i++) RandomSamples[i] = rg.NextDouble();
        }
	    public void AddNode(Node node){
            this.NodeSet.Add(node);
            LinkSet = null;
        }
        public void RemoveNode(Node node, bool isEdgeNode)
        {
            List<Node> nodes = isEdgeNode ? this.LinkSet : this.NodeSet;
            LinkSet = null;
            int idxToDelete = -1;
            for (int i = 0; i < nodes.Count; i++)
            {
                if (((Node)nodes[i]) == node)
                {
                    idxToDelete = i;
                }
            }
            if (idxToDelete >= 0) nodes.RemoveAt(idxToDelete);

            if (isEdgeNode) this.LinkSet = nodes;
            else this.NodeSet = nodes;
        }

        public Graph GetGeometricCommunicationGraph(){
            Graph GCG = this.GetEuclideanGraph();
	        for (int txID = 0; txID < this.NodeSet.Count; txID++){
		        for (int rxID = 0; rxID < this.NodeSet.Count; rxID++){
			        if (GCG.GetWeight(txID, rxID)>((Node)this.NodeSet[txID]).GetTransmissionRange()) GCG.SetWeight(txID, rxID, 0);
		        }
	        }
	        return GCG;
        }
        public Graph GetConflictGraph(Graph LogicalCommunicationGraph)
        {
            if (LogicalCommunicationGraph.GetNodeNumber() != this.NodeSet.Count) throw new NotValidInputException();

            if (this.LinkSet == null)
            {
                //Create one node for each logical link
                this.LinkSet = new List<Node>();
                for (int u = 0; u < LogicalCommunicationGraph.GetNodeNumber(); u++)
                {
                    for (int v = 0; v < LogicalCommunicationGraph.GetNodeNumber(); v++)
                    {
                        if (u != v)
                        {
                            if (LogicalCommunicationGraph.EdgeExists(u, v))
                            {
                                Node n = new Node();
                                n.SetName(((Node)NodeSet[u]).GetName() + ">" + ((Node)NodeSet[v]).GetName());
                                LinkSet.Add(n);
                            }
                        }
                    }
                }
                //Place the nodes on a grid with some randomness to avoid multiple arrows on top of eachother
                int gridSize = (int)Math.Ceiling(Math.Sqrt(LinkSet.Count));
                for (int i = 0; i < LinkSet.Count; i++)
                {
                    Node n = (Node)LinkSet[i];
                    int row = -gridSize/2 + i / gridSize;
                    int col = -gridSize/2 + i % gridSize;

                    double RandX = 1 * RandomSamples[(2 * i) % RandomSamples.Length];
                    double RandY = 1 * RandomSamples[(2 * i + 1) % RandomSamples.Length];
                    n.SetPosition(new Vec3D(col * 2.0 + RandX, row * 2.0 + RandY, 0));
                }
            }

            Graph IG = this.GetInterferenceGraph(LogicalCommunicationGraph);
            Graph CG = new Graph(this.LinkSet.Count);

            for (int j = 0; j < this.LinkSet.Count; j++)
            {
                for (int k = 0; k < this.LinkSet.Count; k++)
                {
                    if (j != k)
                    {
                        //Get node indexes
                        String[] names_j = ((Node)this.LinkSet[j]).GetName().Split('>');
                        String[] names_k = ((Node)this.LinkSet[k]).GetName().Split('>');

                        int src_j=0, src_k=0, dst_j=0, dst_k = 0;
                        bool nodeInCommon = false;
                        for (int nIdx = 0; nIdx < this.NodeSet.Count; nIdx++)
                        {
                            int cnt = 0;
                            Node n = (Node)this.NodeSet[nIdx];
                            if (n.GetName() == names_j[0])
                            {
                                cnt++;
                                src_j = nIdx;
                            }
                            if (n.GetName() == names_j[1]) 
                            { 
                                cnt++; 
                                dst_j = nIdx; 
                            }
                            if (n.GetName() == names_k[0]) 
                            { 
                                cnt++; 
                                src_k = nIdx; 
                            }
                            if (n.GetName() == names_k[1]) 
                            { 
                                cnt++; 
                                dst_k = nIdx; 
                            }
                            nodeInCommon |= (cnt > 1);
                        }

                        //Check for edges
                        //Note: && (src_k!=dst_k) does not make any sense considering nodeInCommon later, I put it to decouple the concept of nodeInCommon and the definition of Conflict Graph
                        if ((IG.EdgeExists(src_j, dst_k) && (src_j!=dst_k)) || nodeInCommon) CG.SetWeight(j, k, 1);

                    }
                }
            }

            return CG;
        }
        public Graph GetInterferenceGraph(Graph LogicalCommunicationGraph)
        {
            if (LogicalCommunicationGraph.GetNodeNumber() != this.NodeSet.Count) throw new NotValidInputException();
            if (this.NodeSet.Count < 2) return new Graph(this.NodeSet.Count);

            Graph IG = new Graph(this.NodeSet.Count);

            //Main loop (1 per edge)
            for (int i = 0; i < this.NodeSet.Count; i++)    //Potential interferer
            {
                for (int r = 0; r < this.NodeSet.Count; r++)    //Receiver
                {
                    if (r != i) //Ignore self loops
                    {
                        Node interferer = (Node)this.NodeSet[i];
                        Node receiver = (Node)this.NodeSet[r];
                        double InterferencePower = interferer.GetTransmitPower() - this.K - 10 * this.beta * Math.Log10((receiver.GetPosition()-interferer.GetPosition()).module());

                        //Find worst case useful transmitter (lowest received power)
                        double ReceivedPower = double.PositiveInfinity;
                        for (int wct = 0; wct < this.NodeSet.Count; wct++)
                        {
                            if (wct != i)
                            {
                                if (LogicalCommunicationGraph.EdgeExists(wct, r))
                                {
                                    Node tmp = (Node)this.NodeSet[wct];
                                    double tmpRxPwr = tmp.GetTransmitPower() - this.K - 10 * this.beta * Math.Log10((receiver.GetPosition() - tmp.GetPosition()).module());
                                    if (tmpRxPwr < ReceivedPower)
                                    {
                                        ReceivedPower = tmpRxPwr;
                                    }
                                }
                            }
                        }

                        if (ReceivedPower - InterferencePower < receiver.GetProtectionRatio())
                        {
                            IG.SetWeight(i, r, 1);
                        }
                    }
                }
            }

            return IG;
        }
	    public Graph GetEuclideanGraph(){
            Graph result = new Graph(this.NodeSet.Count);
	        for (int txID = 0; txID < this.NodeSet.Count; txID++){
		        for (int rxID = 0; rxID < this.NodeSet.Count; rxID++){
			        result.SetWeight(txID, rxID, (((Node)this.NodeSet[txID]).GetPosition() - ((Node)this.NodeSet[rxID]).GetPosition()).module());
		        }
	        }
	        return result;
        }
	    public Graph GetPhysicalCommunicationGraph(double K, double beta){
            Graph PCG = this.GetEuclideanGraph();

	        for (int txID = 0; txID < this.NodeSet.Count; txID++){
		        for (int rxID = 0; rxID < this.NodeSet.Count; rxID++){
			        double Prx = ((Node)this.NodeSet[txID]).GetTransmitPower() - K - 10 * beta*Math.Log10(PCG.GetWeight(txID, rxID));
			        if (Prx > ((Node)this.NodeSet[rxID]).GetReceiverSensitivity()) PCG.SetWeight(txID, rxID, 1);
			        else PCG.SetWeight(txID, rxID, 0);
		        }
	        }
	        return PCG;
        }

	    public int GetEdgeConnectivity(){throw new NotImplementedException();}
	    public int GetNodeConnectivity(){throw new NotImplementedException();}

	    private void updateTransmissionRange()
        {
            LinkSet = null;
            foreach (Node n in this.NodeSet)
            {
                n.SetTransmitPower(n.GetTransmitPower(),K,beta);
            }
        }
        public void SetChannelPathlossModel(double K, double beta)
        {
            if(K <= 0 || beta <= 0)
            {
                throw new InvalidOperationException("K and beta must be > 0");
            }
            this.K = K;
            this.beta = beta;
            updateTransmissionRange();
        }
	    public void SetAllReceiverSensitivities(double receiver_sensitivity){
            foreach(Node n in this.NodeSet){
		        n.SetReceiverSensitivity(receiver_sensitivity,this.K,this.beta);
	        }
            updateTransmissionRange();
        }
	    public void SetAllTransmitPowers(double transmit_power){
            foreach(Node n in this.NodeSet){
		        n.SetTransmitPower(transmit_power,this.K,this.beta);
	        }
            updateTransmissionRange();
        }
        public void SetAllProtectionRatios(double protection_ratio)
        {
            LinkSet = null;
            foreach (Node n in this.NodeSet)
            {
                n.SetProtectionRatio(protection_ratio);
            }
        }
        public double GetChannelK()
        {
            return this.K;
        }
        public double GetChannelBeta()
        {
            return this.beta;
        }
    }

}
