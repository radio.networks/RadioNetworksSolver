﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Collections;

namespace RadioNetworksGraphViewer
{
    class GraphDrawer
    {
        public float scale;
        public Point center;
        private int width;
        private int height;
        public int NodeRadius;

        public GraphDrawer()
        {
            NodeRadius = 20;
        }

        public void Initialize(Panel panel, float scale)
        {
            this.width = (int)panel.Width;
            this.height = (int)panel.Height;
            this.scale = scale;
            this.center = new Point(width / 2, height / 2);
        }

        public Point FromSpaceToGraphic(Vec3D p)
        {
            return new Point((int)(this.scale * p.x + this.center.X), (int)(this.center.Y - this.scale * p.y));
        }

        public Vec3D FromGraphicToSpace(Point p)
        {
            return new Vec3D((p.X - this.center.X) / this.scale, (this.center.Y - p.Y) / this.scale, 0);
        }

        public Node selectedNode(Point mousePosition, List<Node> NodeSet)
        {
            foreach (Node n in NodeSet)
            {
                Point nodeCenter = FromSpaceToGraphic(n.GetPosition());
                int distance2 = (int)(Math.Pow(nodeCenter.X - mousePosition.X, 2) + Math.Pow(nodeCenter.Y - mousePosition.Y, 2));
                if (distance2 < (NodeRadius * NodeRadius)) return n;
            }

            return null;
        }

        private int ScaleLength(int length)
        {
            return (int)scale * length;
        }

        public void Zoom(Point mouse, float scale)
        {
            this.scale *= scale;

            // Point mouseSpace = FromGraphicToSpace(mouse);
            float old_d_x = this.center.X - mouse.X;
            float old_d_y = this.center.Y - mouse.Y;

            this.center.X = (int)(mouse.X + scale * old_d_x);
            this.center.Y = (int)(mouse.Y + scale * old_d_y);
        }

        public void AdjustView(int width, int height, List<Node> nodes)
        {
            int margin = 150;
            double minX, minY, maxX, maxY;
            minX = double.PositiveInfinity;
            minY = double.PositiveInfinity;
            maxX = double.NegativeInfinity;
            maxY = double.NegativeInfinity;



            //find size of the graph
            foreach (Node n in nodes)
            {
                if (n.GetPosition().x < minX) minX = n.GetPosition().x;
                if (n.GetPosition().x > maxX) maxX = n.GetPosition().x;
                if (n.GetPosition().y < minY) minY = n.GetPosition().y;
                if (n.GetPosition().y > maxY) maxY = n.GetPosition().y;
            }

            double scaleX, scaleY;

            //fix weird cases
            if (maxX == minX) scaleX = double.PositiveInfinity;
            else scaleX = (width - 2 * margin) / (maxX - minX);

            if (maxY == minY) scaleY = double.PositiveInfinity;
            else scaleY = (height - 2 * margin) / (maxY - minY);

            if (scaleX == double.PositiveInfinity && scaleY == double.PositiveInfinity)
            {
                //only 1 node
                this.center = FromSpaceToGraphic(new Vec3D(minX, minY, 0));
            }
            else
            {
                if (scaleX > 0 || scaleY > 0)
                {
                    //more than 1 node
                    this.scale = (float)Math.Min(scaleX, scaleY);

                    double min_C_X = minX * scale;
                    double max_C_X = width - maxX * scale;
                    double max_C_Y = -maxY * scale;
                    double min_C_Y = height + minY * scale;

                    this.center.X = (int)(max_C_X - min_C_X) / 2;
                    this.center.Y = -(int)(max_C_Y - min_C_Y) / 2;
                }
            }

            //0 nodes (do nothing)
        }

        public void DrawNode(Graphics g, Node n, bool showTransmissionRange, bool isDisabled)
        {
            Point nodePosition = FromSpaceToGraphic(n.GetPosition());

            if (showTransmissionRange)
            {
                g.DrawEllipse(Pens.Green, new Rectangle(new Point((int)(nodePosition.X - n.GetTransmissionRange() * scale), (int)(nodePosition.Y - n.GetTransmissionRange() * scale)), new Size((int)(2 * n.GetTransmissionRange() * scale), (int)(2 * n.GetTransmissionRange() * scale))));
            }

            if (isDisabled)
            {
                g.FillEllipse(Brushes.DarkGray, new Rectangle(new Point(nodePosition.X - NodeRadius, nodePosition.Y - NodeRadius), new Size(2 * NodeRadius, 2 * NodeRadius)));
                g.FillEllipse(Brushes.Gray, new Rectangle(new Point(nodePosition.X - NodeRadius + 3, nodePosition.Y - NodeRadius + 3), new Size(2 * (NodeRadius - 3), 2 * (NodeRadius - 3))));
            }
            else
            {
                g.FillEllipse(Brushes.DarkBlue, new Rectangle(new Point(nodePosition.X - NodeRadius, nodePosition.Y - NodeRadius), new Size(2 * NodeRadius, 2 * NodeRadius)));
                g.FillEllipse(Brushes.Blue, new Rectangle(new Point(nodePosition.X - NodeRadius + 3, nodePosition.Y - NodeRadius + 3), new Size(2 * (NodeRadius - 3), 2 * (NodeRadius - 3))));
            }

            int length = n.GetName().Length;

            int size = 7 + (int)(length > 0 ? 8 / length : 10);
            int posX = 9 + (length - 1) * 2;
            int posY = 11 - (length - 1) * 2;

            Font font = new Font(FontFamily.GenericSansSerif, size);
            Point position = FromSpaceToGraphic(n.GetPosition());
            position.X -= posX;
            position.Y -= posY;
            g.DrawString(n.GetName(), font, Brushes.GhostWhite, position);


        }

        public void DrawGraph(Graphics g, bool showTransmissionRange, bool showAxis, List<Node> nodes, Graph graph)
        {
            Pen p = new Pen(Color.Black);
            p.Width = 2;
            AdjustableArrowCap bigArrow = new AdjustableArrowCap(5, 5);
            p.CustomEndCap = bigArrow;
            int N = graph.GetNodeNumber();
            for (int u = 0; u < N; u++)
            {
                for (int v = 0; v < N; v++)
                {
                    if (v != u)
                    {
                        if (graph.EdgeExists(u, v))
                        {
                            Point from = FromSpaceToGraphic(((Node)nodes[u]).GetPosition());
                            Point to = FromSpaceToGraphic(((Node)nodes[v]).GetPosition());

                            double angle;
                            try
                            {
                                angle = Math.Atan((1.0 * to.Y - 1.0 * from.Y) / (1.0 * to.X - 1.0 * from.X));
                            }
                            catch (Exception ex)
                            {
                                angle = 0;
                            }
                            if (from.X < to.X) g.DrawLine(p, new Point((int)(from.X + NodeRadius * Math.Cos(angle)), (int)(from.Y + NodeRadius * Math.Sin(angle))), new Point((int)(to.X - NodeRadius * Math.Cos(angle)), (int)(to.Y - NodeRadius * Math.Sin(angle))));
                            else g.DrawLine(p, new Point((int)(from.X - NodeRadius * Math.Cos(angle)), (int)(from.Y - NodeRadius * Math.Sin(angle))), new Point((int)(to.X + NodeRadius * Math.Cos(angle)), (int)(to.Y + NodeRadius * Math.Sin(angle))));
                        }
                    }
                }
            }


            DrawNodes(g, showTransmissionRange, showAxis, nodes);
        }

        public void DrawNodes(Graphics g, bool showTransmissionRange, bool showAxis, List<Node> nodes)
        {
            g.SmoothingMode = SmoothingMode.AntiAlias;
            Pen p = new Pen(Color.Black);
            p.Width = 1;

            if (showAxis)
            {
                int AxisLength = 50;
                g.DrawLine(p, new Point(this.center.X - 5, this.center.Y), new Point(this.center.X + AxisLength, this.center.Y));
                g.DrawLine(p, new Point(this.center.X, this.center.Y + 5), new Point(this.center.X, this.center.Y - AxisLength));

                g.DrawLine(p, new Point(this.center.X - 3, this.center.Y - AxisLength + 5), new Point(this.center.X, this.center.Y - AxisLength));
                g.DrawLine(p, new Point(this.center.X + 3, this.center.Y - AxisLength + 5), new Point(this.center.X, this.center.Y - AxisLength));

                g.DrawLine(p, new Point(this.center.X + AxisLength - 5, this.center.Y - 3), new Point(this.center.X + AxisLength, this.center.Y));
                g.DrawLine(p, new Point(this.center.X + AxisLength - 5, this.center.Y + 3), new Point(this.center.X + AxisLength, this.center.Y));
            }

            //Draw Scale

            //Draw Graph
            foreach (Node n in nodes)
            {
                DrawNode(g, n, showTransmissionRange, false);
            }
        }
    }
}
