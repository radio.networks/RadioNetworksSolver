﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Collections;

namespace RadioNetworksGraphViewer
{
    public partial class Form1 : Form
    {
        enum GUI_Status
        {
            IDLE,
            PANNING,
            ADDING_NODE,
            REMOVING_NODE,
            MOVING_NODE,
        }

        Network net = new Network();
        GUI_Status status;
        List<Node> NodesInView;
        Node temporaryNode = new Node();
        Point PanningRelativePosition;

        public Form1()
        {
            InitializeComponent();
            somethingChanged = false;
            status = GUI_Status.IDLE;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            gd.Initialize(this.graphicPanel, 0.05f);
            try
            {
                net.SetAllProtectionRatios(double.Parse(txt_ProtectionRatio.Text.Replace(',', '.')));
                net.SetAllReceiverSensitivities(double.Parse(txt_RxSens.Text.Replace(',', '.')));
                net.SetAllTransmitPowers(double.Parse(txt_TxPower.Text.Replace(',', '.')));
                net.SetChannelPathlossModel(double.Parse(txt_K.Text.Replace(',', '.')), double.Parse(txt_beta.Text.Replace(',', '.')));
                refreshTransmissionRange();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Wrong Sintax");
            }
        }

        private void refreshTransmissionRange()
        {
            try
            {
                this.lbl_TR.Text = "Transmission Range = " + Math.Pow(10, (double.Parse(txt_TxPower.Text.Replace(',', '.')) - net.GetChannelK() - double.Parse(txt_RxSens.Text.Replace(',', '.'))) / (10 * net.GetChannelBeta())).ToString("G3") + " m";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Wrong Sintax");
            }
        }
        private void graphicPanel_MouseHover(object sender, EventArgs e)
        {
            graphicPanel.Focus();
        }
        private void graphicPanel_MouseWheel(object sender, MouseEventArgs e)
        {
            gd.Zoom(new Point(e.X, e.Y), (float)Math.Pow(2, 1.0 * e.Delta / 300));
            somethingChanged = true;
        }

        private void graphicPanel_MouseDown(object sender, MouseEventArgs e)
        {
            switch (status)
            {
                case GUI_Status.IDLE:
                    //Checking if hit a node
                    Node n = gd.selectedNode(new Point(e.X, e.Y), NodesInView);
                    if (n != null)
                    {
                        status = GUI_Status.MOVING_NODE;
                        net.RemoveNode(n, net.LinkSet == NodesInView);
                        temporaryNode = n;
                        return;
                    }

                    //if not...
                    PanningRelativePosition.X = gd.center.X - e.X;
                    PanningRelativePosition.Y = gd.center.Y - e.Y;
                    status = GUI_Status.PANNING;
                    break;
                case GUI_Status.ADDING_NODE:
                    Node newNode = new Node();
                    newNode.SetPosition(temporaryNode.GetPosition());
                    //newNode.SetTransmissionRange(temporaryNode.GetTransmissionRange());

                    newNode.SetTransmitPower(temporaryNode.GetTransmitPower(), net.GetChannelK(), net.GetChannelBeta());
                    newNode.SetReceiverSensitivity(temporaryNode.GetReceiverSensitivity(), net.GetChannelK(), net.GetChannelBeta());
                    newNode.SetProtectionRatio(temporaryNode.GetProtectionRatio());

                    //Find a name
                    //int i = 0;
                    bool[] free = new bool[net.NodeSet.Count];
                    for (int i = 0; i < net.NodeSet.Count; i++)
                    {
                        free[i] = true;
                        foreach (Node nd in net.NodeSet)
                        {
                            free[i] &= !nd.GetName().Equals(char.ConvertFromUtf32(i + 'A'));
                        }
                    }
                    int nameIdx = 0;
                    for (; nameIdx < free.Length; nameIdx++)
                    {
                        if (free[nameIdx])
                        {
                            newNode.SetName(char.ConvertFromUtf32(nameIdx + 'A'));
                            break;
                        }
                    }

                    if (net.NodeSet.Count == 0) newNode.SetName("A");
                    if (nameIdx == net.NodeSet.Count) newNode.SetName(char.ConvertFromUtf32(nameIdx + 'A'));

                    net.AddNode(newNode);
                    somethingChanged = true;
                    status = GUI_Status.IDLE;
                    break;
                case GUI_Status.REMOVING_NODE:
                    Node nodeToDelete = gd.selectedNode(new Point(e.X, e.Y), net.NodeSet);
                    if (nodeToDelete != null)
                    {
                        net.RemoveNode(nodeToDelete, false);
                        somethingChanged = true;
                        status = GUI_Status.IDLE;
                    }
                    break;
                default:
                    break;
            }


        }

        private void graphicPanel_MouseUp(object sender, MouseEventArgs e)
        {
            switch (status)
            {
                case GUI_Status.PANNING:
                    status = GUI_Status.IDLE;
                    break;
                case GUI_Status.MOVING_NODE:
                    Node n = new Node();
                    n.SetName(temporaryNode.GetName());
                    n.SetPosition(temporaryNode.GetPosition());

                    n.SetTransmitPower(temporaryNode.GetTransmitPower(), net.GetChannelK(), net.GetChannelBeta());
                    n.SetReceiverSensitivity(temporaryNode.GetReceiverSensitivity(), net.GetChannelK(), net.GetChannelBeta());
                    n.SetProtectionRatio(temporaryNode.GetProtectionRatio());

                    if (NodesInView == net.NodeSet) net.NodeSet.Add(n);
                    else net.LinkSet.Add(n);

                    //net.AddNode(n);
                    somethingChanged = true;
                    status = GUI_Status.IDLE;
                    break;
                default:
                    break;
            }
        }

        private void graphicPanel_MouseMove(object sender, MouseEventArgs e)
        {
            switch (status)
            {
                case GUI_Status.PANNING:
                    gd.center.X = e.X + PanningRelativePosition.X;
                    gd.center.Y = e.Y + PanningRelativePosition.Y;
                    somethingChanged = true;
                    break;
                case GUI_Status.ADDING_NODE:
                    temporaryNode.SetName("");

                    temporaryNode.SetPosition(gd.FromGraphicToSpace(new Point(e.X, e.Y)));
                    try
                    {
                        temporaryNode.SetTransmitPower(double.Parse(txt_TxPower.Text), double.Parse(txt_K.Text), double.Parse(txt_beta.Text));
                        temporaryNode.SetReceiverSensitivity(double.Parse(txt_RxSens.Text), double.Parse(txt_K.Text), double.Parse(txt_beta.Text));
                        temporaryNode.SetProtectionRatio(double.Parse(txt_ProtectionRatio.Text));
                        somethingChanged = true;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Wrong Sintax");
                    }
                    break;
                case GUI_Status.MOVING_NODE:
                    temporaryNode.SetPosition(gd.FromGraphicToSpace(new Point(e.X, e.Y)));
                    somethingChanged = true;
                    break;
                default:
                    break;
            }
        }

        #region DRAWING
        GraphDrawer gd = new GraphDrawer();
        bool somethingChanged;
        bool doViewAdjust = false;
        private void graphicPanel_Paint(object sender, PaintEventArgs e)
        {
            // Scale calculation variables
            int MaxLength = 200;
            int exponent = -10;
            for (; exponent < 10; exponent++) if (Math.Pow(10, exponent) * gd.scale > MaxLength / 10 && Math.Pow(10, exponent) * gd.scale < MaxLength) { break; }

            double unit = Math.Pow(10, exponent);
            // griglia
            Pen p = new Pen(Color.LightGray);
            if (!rb_CG.Checked)
            {
                int nColumns = (graphicPanel.Width / (int)(gd.scale * unit));
                int nRows = (graphicPanel.Height / (int)(gd.scale * unit));
                for (int i = 1; i <= nColumns; i++)
                {
                    e.Graphics.DrawLine(p, 0, (int)(gd.scale * unit) * i, graphicPanel.Width, (int)(gd.scale * unit) * i);
                }
                for (int i = 1; i <= nRows; i++)
                {
                    e.Graphics.DrawLine(p, (int)(gd.scale * unit) * i, 0, (int)(gd.scale * unit) * i, graphicPanel.Height);
                }

                //Scale indicator


                p = new Pen(Color.Black);

                e.Graphics.DrawLine(p, 50, 550, 50 + (int)(gd.scale * unit), 550);
                e.Graphics.DrawLine(p, 50, 555, 50, 545);
                e.Graphics.DrawLine(p, 50 + (int)(gd.scale * unit), 555, 50 + (int)(gd.scale * unit), 545);
                Font f = new Font(FontFamily.GenericSansSerif, 10);
                e.Graphics.DrawString(unit + " m", f, Brushes.Black, 60 + (int)(gd.scale * unit), 543);

            }

            if (rb_Nodes.Checked)
            {
                NodesInView = net.NodeSet;
                if (doViewAdjust)
                {
                    gd.AdjustView(graphicPanel.Width, graphicPanel.Height, NodesInView);
                    doViewAdjust = false;
                }

                gd.DrawNodes(e.Graphics, cb_TransmissionRange.Checked, false, NodesInView);
            }
            if (rb_GCG.Checked)
            {
                NodesInView = net.NodeSet;
                if (doViewAdjust)
                {
                    gd.AdjustView(graphicPanel.Width, graphicPanel.Height, NodesInView);
                    doViewAdjust = false;
                }

                gd.DrawGraph(e.Graphics, cb_TransmissionRange.Checked, false, NodesInView, net.GetGeometricCommunicationGraph());
            }
            try
            {
                if (rb_lcg_w_Distance.Checked)
                {
                    NodesInView = net.NodeSet;
                    if (doViewAdjust)
                    {
                        gd.AdjustView(graphicPanel.Width, graphicPanel.Height, NodesInView);
                        doViewAdjust = false;
                    }

                    gd.DrawGraph(e.Graphics, cb_TransmissionRange.Checked, false, NodesInView, net.GetGeometricCommunicationGraph().GetMinimumSpanningTreeDistances());
                }
                if (rb_lcg_w_RxPower.Checked)
                {
                    NodesInView = net.NodeSet;
                    if (doViewAdjust)
                    {
                        gd.AdjustView(graphicPanel.Width, graphicPanel.Height, NodesInView);
                        doViewAdjust = false;
                    }

                    gd.DrawGraph(e.Graphics, cb_TransmissionRange.Checked, false, NodesInView, net.GetGeometricCommunicationGraph().GetMinimumSpanningTreeRxPowers(net));
                }

                if (rb_lcg_w_1.Checked)
                {
                    NodesInView = net.NodeSet;
                    if (doViewAdjust)
                    {
                        gd.AdjustView(graphicPanel.Width, graphicPanel.Height, NodesInView);
                        doViewAdjust = false;
                    }

                    gd.DrawGraph(e.Graphics, cb_TransmissionRange.Checked, false, NodesInView, net.GetGeometricCommunicationGraph().GetMinimumSpanningTreeWeightOne());
                }

            }
            catch (OperationNotPossibleException ex)
            {
                MessageBox.Show("The Graph must be symmetrical");
                rb_lcg_w_Distance.Checked = false;
                rb_lcg_w_1.Checked = false;
                rb_lcg_w_RxPower.Checked = false;
                rb_Nodes.Checked = true;
            }
            if (rb_InterferenceGraph.Checked)
            {
                NodesInView = net.NodeSet;
                if (doViewAdjust)
                {
                    gd.AdjustView(graphicPanel.Width, graphicPanel.Height, NodesInView);
                    doViewAdjust = false;
                }

                gd.DrawGraph(e.Graphics, cb_TransmissionRange.Checked, false, NodesInView, net.GetInterferenceGraph(net.GetGeometricCommunicationGraph().GetMinimumSpanningTreeDistances()));
            }
            if (rb_CG.Checked)
            {

                Graph CG = net.GetConflictGraph(net.GetGeometricCommunicationGraph().GetMinimumSpanningTreeDistances());
                //gd.AdjustView(graphicPanel.Width, graphicPanel.Height,net.LinkSet);
                NodesInView = net.LinkSet;
                gd.AdjustView(graphicPanel.Width, graphicPanel.Height, NodesInView);
                doViewAdjust = true;

                gd.DrawGraph(e.Graphics, cb_TransmissionRange.Checked, false, NodesInView, CG);
            }

            if (status == GUI_Status.ADDING_NODE || status == GUI_Status.MOVING_NODE)
            {
                gd.DrawNode(e.Graphics, temporaryNode, true, true);
            }

            base.OnPaint(e);
        }

        private void tmr_refresh_Tick(object sender, EventArgs e)
        {
            switch (status)
            {
                case GUI_Status.IDLE:
                    btn_Add.Enabled = NodesInView == net.NodeSet;
                    btn_Delete.Enabled = NodesInView == net.NodeSet;
                    btn_Cancel.Enabled = false;
                    break;
                case GUI_Status.ADDING_NODE:
                    btn_Add.Enabled = false;
                    btn_Delete.Enabled = false;
                    btn_Cancel.Enabled = true;
                    break;
                case GUI_Status.REMOVING_NODE:
                    btn_Add.Enabled = false;
                    btn_Delete.Enabled = false;
                    btn_Cancel.Enabled = true;
                    break;
                case GUI_Status.PANNING:
                    btn_Add.Enabled = NodesInView == net.NodeSet;
                    btn_Delete.Enabled = NodesInView == net.NodeSet;
                    btn_Cancel.Enabled = false;
                    break;
                case GUI_Status.MOVING_NODE:
                    btn_Add.Enabled = NodesInView == net.NodeSet;
                    btn_Delete.Enabled = NodesInView == net.NodeSet;
                    btn_Cancel.Enabled = false;
                    break;

            }


            if (somethingChanged)
            {
                this.graphicPanel.Invalidate();
                somethingChanged = false;
            }
        }

        #endregion


        private void cb_TransmissionRange_CheckedChanged(object sender, EventArgs e)
        {
            somethingChanged = true;
        }

        private void btn_Add_Click(object sender, EventArgs e)
        {
            switch (status)
            {
                case GUI_Status.IDLE:
                    status = GUI_Status.ADDING_NODE;
                    break;
            }
        }

        private void btn_Delete_Click(object sender, EventArgs e)
        {
            switch (status)
            {
                case GUI_Status.IDLE:
                    status = GUI_Status.REMOVING_NODE;
                    break;
            }
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            switch (status)
            {
                case GUI_Status.ADDING_NODE:
                    status = GUI_Status.IDLE;
                    break;
                case GUI_Status.REMOVING_NODE:
                    status = GUI_Status.IDLE;
                    break;
            }
        }

        private void cb_GraphType_SelectedIndexChanged(object sender, EventArgs e)
        {
            somethingChanged = true;
        }

        private void rb_Nodes_CheckedChanged(object sender, EventArgs e)
        {
            somethingChanged = true;
        }

        private void rb_GCG_CheckedChanged(object sender, EventArgs e)
        {
            somethingChanged = true;
        }

        private void rb_LG_CheckedChanged(object sender, EventArgs e)
        {
            somethingChanged = true;
        }

        private void btn_ZoomAdj_Click(object sender, EventArgs e)
        {
            gd.AdjustView(graphicPanel.Width, graphicPanel.Height, NodesInView);
            somethingChanged = true;
        }

        private void txt_TxPower_TextChanged(object sender, EventArgs e)
        {
            if (txt_TxPower.Text.Replace(',', '.') != "" && txt_TxPower.Text.Replace(',', '.') != " " && txt_TxPower.Text.Replace(',', '.') != "-")
            {
                somethingChanged = true;
                try
                {
                    net.SetAllTransmitPowers(double.Parse(txt_TxPower.Text.Replace(',', '.')));
                    refreshTransmissionRange();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Transmit power must be a number");
                }
            }
        }
        private void txt_RxSens_TextChanged(object sender, EventArgs e)
        {
            if (txt_RxSens.Text.Replace(',', '.') != "" && txt_RxSens.Text.Replace(',', '.') != " " && txt_RxSens.Text.Replace(',', '.') != "-")
            {
                somethingChanged = true;
                try
                {
                    net.SetAllReceiverSensitivities(double.Parse(txt_RxSens.Text.Replace(',', '.')));
                    refreshTransmissionRange();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Receiver Sensitivity must be a number");
                }
            }
        }
        private void txt_ProtectionRatio_TextChanged(object sender, EventArgs e)
        {
            String input = txt_ProtectionRatio.Text.Replace(".", System.Globalization.CultureInfo.CurrentUICulture.NumberFormat.NumberDecimalSeparator);
            input = input.Replace(",", System.Globalization.CultureInfo.CurrentUICulture.NumberFormat.NumberDecimalSeparator);
            if (input != "" && input != " " && input != "-")
            {
                somethingChanged = true;
                try
                {
                    net.SetAllProtectionRatios(double.Parse(input));
                    refreshTransmissionRange();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Protection Ratio must be a number");
                }
            }
        }
        private void txt_K_TextChanged(object sender, EventArgs e)
        {

            String input = txt_K.Text.Replace(".", System.Globalization.CultureInfo.CurrentUICulture.NumberFormat.NumberDecimalSeparator);
            input = input.Replace(",", System.Globalization.CultureInfo.CurrentUICulture.NumberFormat.NumberDecimalSeparator);
            if (input != "" && input != " " && input != "-")
            {
                somethingChanged = true;
                try
                {
                    net.SetChannelPathlossModel(double.Parse(input), net.GetChannelBeta());
                    refreshTransmissionRange();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("K must be a number");
                }
            }
        }
        private void txt_beta_TextChanged(object sender, EventArgs e)
        {
            String input = txt_beta.Text.Replace(".", System.Globalization.CultureInfo.CurrentUICulture.NumberFormat.NumberDecimalSeparator);
            input = input.Replace(",", System.Globalization.CultureInfo.CurrentUICulture.NumberFormat.NumberDecimalSeparator);
            if (input != "" && input != " " && input != "-")
            {
                somethingChanged = true;
                try
                {
                    net.SetChannelPathlossModel(net.GetChannelK(), double.Parse(input));
                    refreshTransmissionRange();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Beta must be a number > 0");
                }
            }
        }

        private void rb_InterferenceGraph_CheckedChanged(object sender, EventArgs e)
        {
            somethingChanged = true;
        }

        private void rb_CG_CheckedChanged(object sender, EventArgs e)
        {
            somethingChanged = true;
        }

        private void rb_Nodes_CheckedChanged_1(object sender, EventArgs e)
        {
            somethingChanged = true;
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void loadButton_Click(object sender, EventArgs e)
        {
            // Displays an OpenFileDialog so the user can select a Cursor.  
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "RadioNetworksGraph network binary | *.rng";
            openFileDialog1.Title = "Load a Radio Networks Graph network";

            // Show the Dialog.  
            // If the user clicked OK in the dialog and  
            // a .rngbin file was selected, open it.  
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                // Assign the cursor in the Stream to the Form's Cursor property.  
               System.IO.Stream fs = openFileDialog1.OpenFile();
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                net = (Network)binaryFormatter.Deserialize(fs);
                somethingChanged = true;
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "RadioNetworksGraph network binary | *.rng";
            saveFileDialog1.Title = "Save a Radio Networks Graph network";
            saveFileDialog1.ShowDialog();

            // If the file name is not an empty string open it for saving.  
            if (saveFileDialog1.FileName != "")
            {
                // Saves the Image via a FileStream created by the OpenFile method.  
                System.IO.FileStream fs =
                   (System.IO.FileStream)saveFileDialog1.OpenFile();

                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binaryFormatter.Serialize(fs, net);
                fs.Close();
            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            somethingChanged = true;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            somethingChanged = true;
        }

        private void rb_lcg_w_RxPower_CheckedChanged(object sender, EventArgs e)
        {
            somethingChanged = true;
        }
    }
}
