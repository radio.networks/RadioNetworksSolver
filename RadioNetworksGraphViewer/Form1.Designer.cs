﻿namespace RadioNetworksGraphViewer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.graphicPanel = new System.Windows.Forms.Panel();
            this.tmr_refresh = new System.Windows.Forms.Timer(this.components);
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.btn_Delete = new System.Windows.Forms.Button();
            this.btn_Add = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.lbl_TR = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_beta = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txt_K = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_ProtectionRatio = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_RxSens = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_TxPower = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.rb_lcg_w_1 = new System.Windows.Forms.RadioButton();
            this.rb_lcg_w_RxPower = new System.Windows.Forms.RadioButton();
            this.rb_lcg_w_Distance = new System.Windows.Forms.RadioButton();
            this.rb_CG = new System.Windows.Forms.RadioButton();
            this.rb_InterferenceGraph = new System.Windows.Forms.RadioButton();
            this.btn_ZoomAdj = new System.Windows.Forms.Button();
            this.cb_TransmissionRange = new System.Windows.Forms.CheckBox();
            this.rb_Nodes = new System.Windows.Forms.RadioButton();
            this.rb_GCG = new System.Windows.Forms.RadioButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // graphicPanel
            // 
            this.graphicPanel.BackColor = System.Drawing.Color.White;
            this.graphicPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.graphicPanel.Location = new System.Drawing.Point(295, 0);
            this.graphicPanel.Margin = new System.Windows.Forms.Padding(4);
            this.graphicPanel.Name = "graphicPanel";
            this.graphicPanel.Size = new System.Drawing.Size(815, 770);
            this.graphicPanel.TabIndex = 1;
            this.graphicPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.graphicPanel_Paint);
            this.graphicPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.graphicPanel_MouseDown);
            this.graphicPanel.MouseHover += new System.EventHandler(this.graphicPanel_MouseHover);
            this.graphicPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.graphicPanel_MouseMove);
            this.graphicPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.graphicPanel_MouseUp);
            this.graphicPanel.MouseWheel += new System.Windows.Forms.MouseEventHandler(this.graphicPanel_MouseWheel);
            // 
            // tmr_refresh
            // 
            this.tmr_refresh.Enabled = true;
            this.tmr_refresh.Interval = 10;
            this.tmr_refresh.Tick += new System.EventHandler(this.tmr_refresh_Tick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btn_Cancel);
            this.groupBox3.Controls.Add(this.btn_Delete);
            this.groupBox3.Controls.Add(this.btn_Add);
            this.groupBox3.Location = new System.Drawing.Point(13, 58);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox3.Size = new System.Drawing.Size(274, 95);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Graph";
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(6, 59);
            this.btn_Cancel.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(260, 28);
            this.btn_Cancel.TabIndex = 15;
            this.btn_Cancel.Text = "Cancel Command";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // btn_Delete
            // 
            this.btn_Delete.Location = new System.Drawing.Point(139, 23);
            this.btn_Delete.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Delete.Name = "btn_Delete";
            this.btn_Delete.Size = new System.Drawing.Size(127, 28);
            this.btn_Delete.TabIndex = 14;
            this.btn_Delete.Text = "Delete Node";
            this.btn_Delete.UseVisualStyleBackColor = true;
            this.btn_Delete.Click += new System.EventHandler(this.btn_Delete_Click);
            // 
            // btn_Add
            // 
            this.btn_Add.Location = new System.Drawing.Point(8, 23);
            this.btn_Add.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Add.Name = "btn_Add";
            this.btn_Add.Size = new System.Drawing.Size(127, 28);
            this.btn_Add.TabIndex = 13;
            this.btn_Add.Text = "Add Node";
            this.btn_Add.UseVisualStyleBackColor = true;
            this.btn_Add.Click += new System.EventHandler(this.btn_Add_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.lbl_TR);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.txt_beta);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.txt_K);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.txt_ProtectionRatio);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.txt_RxSens);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.label2);
            this.groupBox4.Controls.Add(this.txt_TxPower);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Location = new System.Drawing.Point(16, 161);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(271, 225);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Physical Layer";
            // 
            // lbl_TR
            // 
            this.lbl_TR.AutoSize = true;
            this.lbl_TR.Location = new System.Drawing.Point(4, 194);
            this.lbl_TR.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_TR.Name = "lbl_TR";
            this.lbl_TR.Size = new System.Drawing.Size(178, 17);
            this.lbl_TR.TabIndex = 35;
            this.lbl_TR.Text = "Transmission Range = ???";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(45, 166);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(193, 17);
            this.label7.TabIndex = 34;
            this.label7.Text = "L = K + 10*beta*log_10(d) dB";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // txt_beta
            // 
            this.txt_beta.Location = new System.Drawing.Point(179, 127);
            this.txt_beta.Margin = new System.Windows.Forms.Padding(4);
            this.txt_beta.Name = "txt_beta";
            this.txt_beta.Size = new System.Drawing.Size(83, 22);
            this.txt_beta.TabIndex = 33;
            this.txt_beta.Text = "3.522";
            this.txt_beta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txt_beta.TextChanged += new System.EventHandler(this.txt_beta_TextChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(133, 130);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(36, 17);
            this.label10.TabIndex = 32;
            this.label10.Text = "beta";
            // 
            // txt_K
            // 
            this.txt_K.Location = new System.Drawing.Point(39, 127);
            this.txt_K.Margin = new System.Windows.Forms.Padding(4);
            this.txt_K.Name = "txt_K";
            this.txt_K.Size = new System.Drawing.Size(75, 22);
            this.txt_K.TabIndex = 30;
            this.txt_K.Text = "21.94";
            this.txt_K.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txt_K.TextChanged += new System.EventHandler(this.txt_K_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 130);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(17, 17);
            this.label8.TabIndex = 29;
            this.label8.Text = "K";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(225, 98);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 17);
            this.label5.TabIndex = 28;
            this.label5.Text = "dB";
            // 
            // txt_ProtectionRatio
            // 
            this.txt_ProtectionRatio.Location = new System.Drawing.Point(123, 95);
            this.txt_ProtectionRatio.Margin = new System.Windows.Forms.Padding(4);
            this.txt_ProtectionRatio.Name = "txt_ProtectionRatio";
            this.txt_ProtectionRatio.Size = new System.Drawing.Size(93, 22);
            this.txt_ProtectionRatio.TabIndex = 27;
            this.txt_ProtectionRatio.Text = "1.5";
            this.txt_ProtectionRatio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txt_ProtectionRatio.TextChanged += new System.EventHandler(this.txt_ProtectionRatio_TextChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 98);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 17);
            this.label6.TabIndex = 26;
            this.label6.Text = "Protection Ratio";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(225, 66);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 17);
            this.label3.TabIndex = 25;
            this.label3.Text = "dBm";
            // 
            // txt_RxSens
            // 
            this.txt_RxSens.Location = new System.Drawing.Point(123, 63);
            this.txt_RxSens.Margin = new System.Windows.Forms.Padding(4);
            this.txt_RxSens.Name = "txt_RxSens";
            this.txt_RxSens.Size = new System.Drawing.Size(93, 22);
            this.txt_RxSens.TabIndex = 24;
            this.txt_RxSens.Text = "-135";
            this.txt_RxSens.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txt_RxSens.TextChanged += new System.EventHandler(this.txt_RxSens_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 66);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 17);
            this.label4.TabIndex = 23;
            this.label4.Text = "Rx Sensitivity";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(225, 33);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 17);
            this.label2.TabIndex = 22;
            this.label2.Text = "dBm";
            // 
            // txt_TxPower
            // 
            this.txt_TxPower.Location = new System.Drawing.Point(123, 30);
            this.txt_TxPower.Margin = new System.Windows.Forms.Padding(4);
            this.txt_TxPower.Name = "txt_TxPower";
            this.txt_TxPower.Size = new System.Drawing.Size(93, 22);
            this.txt_TxPower.TabIndex = 21;
            this.txt_TxPower.Text = "14";
            this.txt_TxPower.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txt_TxPower.TextChanged += new System.EventHandler(this.txt_TxPower_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 33);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 17);
            this.label1.TabIndex = 20;
            this.label1.Text = "Tx Power";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.rb_lcg_w_1);
            this.groupBox5.Controls.Add(this.rb_lcg_w_RxPower);
            this.groupBox5.Controls.Add(this.rb_lcg_w_Distance);
            this.groupBox5.Controls.Add(this.rb_CG);
            this.groupBox5.Controls.Add(this.rb_InterferenceGraph);
            this.groupBox5.Controls.Add(this.btn_ZoomAdj);
            this.groupBox5.Controls.Add(this.cb_TransmissionRange);
            this.groupBox5.Controls.Add(this.rb_Nodes);
            this.groupBox5.Controls.Add(this.rb_GCG);
            this.groupBox5.Location = new System.Drawing.Point(16, 394);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(271, 273);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "View";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 75);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(197, 17);
            this.label9.TabIndex = 30;
            this.label9.Text = "Logical Communication Graph";
            // 
            // rb_lcg_w_1
            // 
            this.rb_lcg_w_1.AutoSize = true;
            this.rb_lcg_w_1.Location = new System.Drawing.Point(22, 153);
            this.rb_lcg_w_1.Name = "rb_lcg_w_1";
            this.rb_lcg_w_1.Size = new System.Drawing.Size(62, 21);
            this.rb_lcg_w_1.TabIndex = 29;
            this.rb_lcg_w_1.TabStop = true;
            this.rb_lcg_w_1.Text = "w = 1";
            this.rb_lcg_w_1.UseVisualStyleBackColor = true;
            this.rb_lcg_w_1.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // rb_lcg_w_RxPower
            // 
            this.rb_lcg_w_RxPower.AutoSize = true;
            this.rb_lcg_w_RxPower.Location = new System.Drawing.Point(22, 126);
            this.rb_lcg_w_RxPower.Name = "rb_lcg_w_RxPower";
            this.rb_lcg_w_RxPower.Size = new System.Drawing.Size(114, 21);
            this.rb_lcg_w_RxPower.TabIndex = 28;
            this.rb_lcg_w_RxPower.TabStop = true;
            this.rb_lcg_w_RxPower.Text = "w = -RxPower";
            this.rb_lcg_w_RxPower.UseVisualStyleBackColor = true;
            this.rb_lcg_w_RxPower.CheckedChanged += new System.EventHandler(this.rb_lcg_w_RxPower_CheckedChanged);
            // 
            // rb_lcg_w_Distance
            // 
            this.rb_lcg_w_Distance.AutoSize = true;
            this.rb_lcg_w_Distance.Location = new System.Drawing.Point(22, 99);
            this.rb_lcg_w_Distance.Name = "rb_lcg_w_Distance";
            this.rb_lcg_w_Distance.Size = new System.Drawing.Size(107, 21);
            this.rb_lcg_w_Distance.TabIndex = 27;
            this.rb_lcg_w_Distance.TabStop = true;
            this.rb_lcg_w_Distance.Text = "w = distance";
            this.rb_lcg_w_Distance.UseVisualStyleBackColor = true;
            this.rb_lcg_w_Distance.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // rb_CG
            // 
            this.rb_CG.AutoSize = true;
            this.rb_CG.Location = new System.Drawing.Point(8, 209);
            this.rb_CG.Margin = new System.Windows.Forms.Padding(4);
            this.rb_CG.Name = "rb_CG";
            this.rb_CG.Size = new System.Drawing.Size(119, 21);
            this.rb_CG.TabIndex = 26;
            this.rb_CG.Text = "Conflict Graph";
            this.rb_CG.UseVisualStyleBackColor = true;
            this.rb_CG.CheckedChanged += new System.EventHandler(this.rb_CG_CheckedChanged);
            // 
            // rb_InterferenceGraph
            // 
            this.rb_InterferenceGraph.AutoSize = true;
            this.rb_InterferenceGraph.Location = new System.Drawing.Point(8, 181);
            this.rb_InterferenceGraph.Margin = new System.Windows.Forms.Padding(4);
            this.rb_InterferenceGraph.Name = "rb_InterferenceGraph";
            this.rb_InterferenceGraph.Size = new System.Drawing.Size(149, 21);
            this.rb_InterferenceGraph.TabIndex = 25;
            this.rb_InterferenceGraph.Text = "Interference Graph";
            this.rb_InterferenceGraph.UseVisualStyleBackColor = true;
            this.rb_InterferenceGraph.CheckedChanged += new System.EventHandler(this.rb_InterferenceGraph_CheckedChanged);
            // 
            // btn_ZoomAdj
            // 
            this.btn_ZoomAdj.Location = new System.Drawing.Point(190, 12);
            this.btn_ZoomAdj.Margin = new System.Windows.Forms.Padding(4);
            this.btn_ZoomAdj.Name = "btn_ZoomAdj";
            this.btn_ZoomAdj.Size = new System.Drawing.Size(73, 27);
            this.btn_ZoomAdj.TabIndex = 24;
            this.btn_ZoomAdj.Text = "Adjust";
            this.btn_ZoomAdj.UseVisualStyleBackColor = true;
            this.btn_ZoomAdj.Click += new System.EventHandler(this.btn_ZoomAdj_Click);
            // 
            // cb_TransmissionRange
            // 
            this.cb_TransmissionRange.AutoSize = true;
            this.cb_TransmissionRange.Location = new System.Drawing.Point(7, 244);
            this.cb_TransmissionRange.Margin = new System.Windows.Forms.Padding(4);
            this.cb_TransmissionRange.Name = "cb_TransmissionRange";
            this.cb_TransmissionRange.Size = new System.Drawing.Size(198, 21);
            this.cb_TransmissionRange.TabIndex = 23;
            this.cb_TransmissionRange.Text = "Show Transmission Range";
            this.cb_TransmissionRange.UseVisualStyleBackColor = true;
            this.cb_TransmissionRange.CheckedChanged += new System.EventHandler(this.cb_TransmissionRange_CheckedChanged);
            // 
            // rb_Nodes
            // 
            this.rb_Nodes.AutoSize = true;
            this.rb_Nodes.Checked = true;
            this.rb_Nodes.Location = new System.Drawing.Point(10, 23);
            this.rb_Nodes.Margin = new System.Windows.Forms.Padding(4);
            this.rb_Nodes.Name = "rb_Nodes";
            this.rb_Nodes.Size = new System.Drawing.Size(103, 21);
            this.rb_Nodes.TabIndex = 20;
            this.rb_Nodes.TabStop = true;
            this.rb_Nodes.Text = "Only Nodes";
            this.rb_Nodes.UseVisualStyleBackColor = true;
            this.rb_Nodes.CheckedChanged += new System.EventHandler(this.rb_Nodes_CheckedChanged_1);
            // 
            // rb_GCG
            // 
            this.rb_GCG.AutoSize = true;
            this.rb_GCG.Location = new System.Drawing.Point(10, 49);
            this.rb_GCG.Margin = new System.Windows.Forms.Padding(4);
            this.rb_GCG.Name = "rb_GCG";
            this.rb_GCG.Size = new System.Drawing.Size(225, 21);
            this.rb_GCG.TabIndex = 21;
            this.rb_GCG.Text = "Physical Communication Graph";
            this.rb_GCG.UseVisualStyleBackColor = true;
            this.rb_GCG.CheckedChanged += new System.EventHandler(this.rb_GCG_CheckedChanged);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(41, 674);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(210, 78);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Location = new System.Drawing.Point(13, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(275, 51);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Configuration";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(139, 21);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(127, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Load";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.loadButton_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(8, 21);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(127, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1108, 768);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.graphicPanel);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1126, 815);
            this.MinimumSize = new System.Drawing.Size(1126, 815);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Radionetworks Graphs Viewer v1.0 - by Luca Feltrin, Marco Raminella";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel graphicPanel;
        private System.Windows.Forms.Timer tmr_refresh;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Button btn_Delete;
        private System.Windows.Forms.Button btn_Add;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_TxPower;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btn_ZoomAdj;
        private System.Windows.Forms.CheckBox cb_TransmissionRange;
        private System.Windows.Forms.RadioButton rb_Nodes;
        private System.Windows.Forms.RadioButton rb_GCG;
        private System.Windows.Forms.Label lbl_TR;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txt_beta;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txt_K;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_ProtectionRatio;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_RxSens;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RadioButton rb_InterferenceGraph;
        private System.Windows.Forms.RadioButton rb_CG;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RadioButton rb_lcg_w_1;
        private System.Windows.Forms.RadioButton rb_lcg_w_RxPower;
        private System.Windows.Forms.RadioButton rb_lcg_w_Distance;
        private System.Windows.Forms.Label label9;
    }
}

